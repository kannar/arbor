(
    app-admin/66-exherbo[~scm]
    app-admin/eclectic[~scm]
    app-admin/s6-exherbo[~scm]
    app-arch/libarchive[~scm]
    app-misc/screen[~scm]
    app-editors/e4r[~scm]
    app-shells/bash-completion[~scm]
    app-shells/zsh[~scm]
    app-text/djvu[~scm]
    app-vim/exheres-syntax[~scm]
    dev-db/xapian-core[~scm]
    dev-lang/clang[~scm]
    dev-lang/llvm[~scm]
    dev-libs/compiler-rt[~scm]
    dev-libs/oblibs[~scm]
    sys-libs/openmp[~scm]
    dev-libs/fmt[~scm]
    dev-libs/pinktrace[~scm]
    dev-libs/pugixml[~scm]
    dev-ruby/ruby-filemagic[~scm]
    dev-scm/git-remote-helpers[~scm]
    dev-util/exherbo-dev-tools[~scm]
    dev-util/ltrace[~scm]
    dev-util/systemtap[~scm]
    dev-util/tig[~scm]
    dev-util/valgrind[~scm]
    media-libs/jbig2dec[~scm]
    net-irc/irssi[~scm]
    net-wireless/iw[~scm]
    net-wireless/wireless-regdb[~scm]
    net-wireless/wpa_supplicant[~scm]
    net-www/elinks[~scm]
    sys-apps/66[~scm]
    sys-apps/66-tools[~scm]
    sys-apps/dbus[~scm]
    sys-apps/eudev[~scm]
#    sys-apps/paludis[~scm]
    sys-apps/multiload[~scm]
    sys-apps/sydbox[~scm]
    sys-apps/systemd[~scm]
    sys-boot/dracut[~scm]
    sys-boot/efibootmgr[~scm]
    sys-boot/grub[~scm]
    sys-devel/meson[~scm]
    sys-devel/ninja[~scm]
    sys-fs/btrfs-progs[~scm]
    sys-libs/gcompat[~scm]
    sys-libs/libc++[~scm]
    sys-libs/libc++abi[~scm]
    sys-libs/musl[~scm]
    sys-libs/musl-compat[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

dev-util/ccache [[
    author = [ Thomas Anderson <tanderson@caltech.edu> ]
    date = [ 14 Apr 2015 ]
    token = broken
    description = [
        UNSUPPORTED: Results in subtle, often undetectable breakage. Don't use it to compile packages or you get to keep both pieces.
    ]
]]

dev-libs/openssl[<1.0.2t] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Nov 2019 ]
    token = security
    description = [ CVE-2019-1547, CVE-2019-1563 ]
]]

dev-libs/openssl[>=1.1&<1.1.1g] [[
    author = [ Kevin Decherf <kevin@kdecherf.com> ]
    date = [ 21 Apr 2020 ]
    token = security
    description = [ CVE-2020-1967 ]
]]

media-libs/jasper[<1.900.20] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2016-10251 ]
]]

dev-db/mysql[<5.7.22] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Sep 2018 ]
    token = security
    description = [ Oracle Critical Patch Update Advisory - July 2018 ]
]]

net-misc/curl[<7.66.0] [[
    author = [ Heiko Becker <tgurr@exherbo.org> ]
    date = [ 14 Sep 2019 ]
    token = security
    description = [ CVE-2019-5481, CVE-2019-5482 ]
]]

app-admin/sudo[<1.8.31] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Feb 2020 ]
    token = security
    description = [ CVE-2019-18634 ]
]]

(
    media-libs/libpng:1.2[<1.2.57]
    media-libs/libpng:1.5[<1.5.28]
    media-libs/libpng:1.6[<1.6.27]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 03 Jan 2017 ]
    *token = security
    *description = [ CVE-2016-10087 ]
]]

app-arch/libzip[<1.3.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Sep 2017 ]
    token = security
    description = [ CVE-2017-12858, CVE-2017-14107 ]
]]

dev-libs/expat[<2.2.8] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Sep 2019 ]
    token = security
    description = [ CVE-2019-15903 ]
]]

media-libs/raptor[<=2.0.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Mar 2012 ]
    token = security
    description = [ CVE-2012-0037 ]
]]

(
    dev-lang/python:2.7[<2.7.11-r2]
    dev-lang/python:3.5[<3.5.1-r2]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 12 Jun 2016 ]
    *token = security
    *description = [ http://www.openwall.com/lists/oss-security/2016/06/11/2 ]
]]

dev-lang/python:3.6[<3.6.9] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Sep 2019 ]
    *token = security
    *description = [ CVE-2019-{5070,9740,9947,9948} ]
]]

sys-apps/sydbox[~0-scm] [[
    author = [ Ali Polatel <alip@exherbo.org> ]
    date = [ 14 Jun 2012 ]
    token = scm
    description = [ Mask scm version ]
]]

net-libs/libotr[<=3.2.0] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 30 Aug 2012 ]
    token = security
    description = [ CVE-2012-2369 ]
]]

net-print/cups[<2.3.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Dec 2019 ]
    token = security
    description = [ CVE-2019-2228 ]
]]

dev-libs/dbus-glib[<0.100.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Mar 2013 ]
    token = security
    description = [ CVE-2013-0292 ]
]]

dev-libs/libxml2[<2.9.10-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Feb 2020 ]
    token = security
    description = [ CVE-2019-20388, CVE-2020-7595 ]
]]

media-libs/tiff[<4.0.9-r3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Feb 2018 ]
    token = security
    description = [ CVE-2018-5784 ]
]]

sys-apps/dbus[<1.12.16] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Jun 2019 ]
    token = security
    description = [ CVE-2019-12749 ]
]]

app-crypt/gnupg[<2.2.18] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 Nov 2019 ]
    token = security
    description = [ CVE-2019-14855 ]
]]

(
    dev-libs/icu:57.1[<57.1-r1]
    dev-libs/icu:58.1[<58.2-r1]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 23 May 2017 ]
    *token = security
    *description = [ CVE-2017-7867, CVE-2017-7868 ]
]]

net-misc/openssh[<7.9_p1-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Mar 2019 ]
    token = security
    description = [ CVE-2018-20685, CVE-2019-6109, CVE-2019-6111 ]
]]

dev-utils/ack[<2.12] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Dez 2013 ]
    token = security
    description = [ http://beyondgrep.com/security/ ]
]]

dev-libs/pinktrace[~0-scm] [[
    author = [ Ali Polatel <alip@exherbo.org> ]
    date = [ 09 Jan 2014 ]
    token = scm
    description = [ Mask scm version ]
]]

sys-apps/file[<5.37-r2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Nov 2019 ]
    token = security
    description = [ CVE-2019-18218 ]
]]

dev-libs/gnutls[<3.6.13] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 02 Apr 2020 ]
    token = security
    description = [ GNUTLS-SA-2020-03-31 ]
]]

net-print/cups-filters[<1.4.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Dec 2015 ]
    token = security
    description = [ CVE-2015-8560 ]
]]

app-arch/lzo[<2.07] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Jul 2014 ]
    token = security
    description = [ CVE-2014-4607 ]
]]

net-libs/cyrus-sasl[<2.1.26-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Jul 2014 ]
    token = security
    description = [ CVE-2013-4122 ]
]]

app-crypt/gpgme[<1.5.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Aug 2014 ]
    token = security
    description = [ CVE-2014-3564 ]
]]

dev-libs/libgcrypt[<1.8.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Jun 2018 ]
    token = security
    description = [ CVE-2018-0495 ]
]]

app-shells/bash[<4.3_p30] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Oct 2014 ]
    token = security
    description = [ CVE-2014-6271, CVE-2014-6277, CVE-2014-6278, CVE-2014-7169, CVE-2014-7186, CVE-2014-7187 ]
]]

(
    sys-libs/db:6.1
    sys-libs/db:6.2
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 27 Oct 2014 ]
    *token = testing
    *description = [ Licence changed to AGPL-3, interfering with various packages (e.g. openldap) ]
]]

net-misc/wget[<1.19.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 07 May 2018 ]
    token = security
    description = [ CVE-2018-0494 ]
]]

dev-libs/libksba[<1.3.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Nov 2014 ]
    token = security
    description = [ CVE-2014-9087 ]
]]

net-dns/bind[<9.14.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Nov 2019 ]
    token = security
    description = [ CVE-2019-6477 ]
]]

net/ntp[<4.2.8_p13] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 11 Mar 2019 ]
    token = security
    description = [ CVE-2019-8936 ]
]]

dev-scm/subversion[<1.8.14] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Sep 2015 ]
    token = security
    description = [ CVE-2015-318{4,7} ]
]]

(
    dev-libs/libevent:0[<2.0.22-r3]
    dev-libs/libevent:2.1[<2.1.6]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 23 May 2017 ]
    *token = security
    *description = [ CVE-2016-1019{5,6,7} ]
]]

sys-devel/patch[<2.7.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Feb 2015 ]
    token = security
    description = [ CVE-2015-1196 ]
]]

sys-apps/grep[<2.22] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Nov 2015 ]
    token = security
    description = [ CVE-2015-1345 ]
]]

sys-libs/glibc[<2.30] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Oct 2019 ]
    token = security
    description = [ CVE-2019-9169 ]
]]

sys-fs/e2fsprogs[<1.45.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Sep 2019 ]
    token = security
    description = [ CVE-2019-5094 ]
]]

dev-lang/perl:5.26[<5.26.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 02 Dec 2018 ]
    token = security
    description = [ CVE-2018-{12015,18311,18312,18313,18314 ]
]]

dev-lang/perl:5.28[<5.28.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 02 Dec 2018 ]
    token = security
    description = [ CVE-2018-18311, CVE-2018-18312 ]
]]

media-libs/gd[<2.2.5-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Feb 2019 ]
    token = security
    description = [ CVE-2019-6978 ]
]]

app-arch/libtar[<1.2.20] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Mar 2015 ]
    token = security
    description = [ CVE-2013-4397 ]
]]

net-libs/libssh2[<1.8.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 19 Mar 2019 ]
    token = security
    description = [ CVE-2019-{3855,3856,3857,3858,3859,3860,3861,3862,3863} ]
]]

dev-libs/libtasn1[<4.12-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Sep 2017 ]
    token = security
    description = [ CVE-2017-10790 ]
]]

sys-apps/paludis[~2.4.0] [[
    author = [ Thomas Anderson <tanderson@caltech.edu> ]
    date = [ 15 Apr 2015 ]
    description = [ Doesn't support multiarch, downgrading from scm *will* break your system ]
    token = broken
]]

dev-libs/pcre[<8.40-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jun 2017 ]
    token = security
    description = [ CVE-2017-6004 ]
]]

dev-lang/lua:5.3 [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 May 2015 ]
    token = testing
    description = [ Many packages are incompatible with Lua 5.3 ]
]]

dev-db/sqlite:3[<3.28.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 May 2019 ]
    token = security
    description = [ CVE-2019-5018 ]
]]

app-antivirus/clamav[<0.102.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Feb 2020 ]
    token = security
    description = [ CVE-2020-3123 ]
]]

dev-libs/xerces-c[<3.1.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 May 2015 ]
    token = security
    description = [ CVE-2015-0252 ]
]]

(
    sys-fs/fuse:0[<2.9.8]
    sys-fs/fuse:3[<3.2.5]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 25 Jul 2018 ]
    *token = security
    *description = [ CVE-2018-10906 ]
]]

sys-libs/pam[<1.2.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 27 Jun 2015 ]
    token = security
    description = [ CVE-2015-3238 ]
]]

sys-apps/less[<475] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Jul 2015 ]
    token = security
    description = [ CVE-2014-9488 ]
]]

net-dns/libidn[<1.33-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Sep 2017 ]
    token = security
    description = [ CVE-2017-14062 ]
]]

sys-fs/xfsprogs[<3.2.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Aug 2015 ]
    token = security
    description = [ CVE-2012-2150 ]
]]

net-nds/rpcbind[<0.2.4-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2017-8779 ]
]]

sys-apps/xinetd[<2.3.15-r3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Nov 2015 ]
    token = security
    description = [ CVE-2013-4342 ]
]]

app-arch/unzip[<6.0-r5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Aug 2019 ]
    token = security
    description = [ CVE-2019-13232 ]
]]

sys-apps/systemd[<241] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Feb 2019 ]
    token = security
    description = [ CVE-2018-16864, CVE-2018-16865 ]
]]

app-arch/p7zip[<16.02-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Feb 2018 ]
    token = security
    description = [ CVE-2017-17969, CVE-2018-5996 ]
]]

sys-boot/grub[<2.02-beta2-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Dec 2015 ]
    token = security
    description = [ CVE-2015-8370 ]
]]

dev-libs/libxslt[<1.1.33-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Oct 2019 ]
    token = security
    description = [ CVE-2019-18197 ]
]]

app-misc/screen[<4.8.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Mar 2020 ]
    token = security
    description = [ CVE-2020-9366 ]
]]

dev-libs/botan[<1.10.17] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 04 Oct 2017 ]
    token = security
    description = [ CVE-2016-14737 ]
]]

(
    dev-scm/git[<2.25.4]
    dev-scm/git[>=2.26&<2.26.2]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 20 Apr Dec 2020 ]
    *token = security
    *description = [ CVE-2020-11008 ]
]]

sys-apps/busybox[<1.28.0] [[
    author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    date = [ 26 Mar 2018 ]
    token = security
    description = [ CVE-2017-15873 CVE-2017-15874 CVE-2017-16544 ]
]]

media-gfx/ImageMagick[<6.9.10.56] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Jul 2019 ]
    token = security
    description = [ Multiple CVEs ]
]]

dev-libs/jansson[<2.7-r1] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 05 May 2016 ]
    token = security
    description = [ CVE-2016-4425 ]
]]

net-wireless/wpa_supplicant[<2.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 Jul 2019 ]
    token = security
    description = [ CVE-2019-949{4,5,9} ]
]]

net-misc/openntpd[<6.0_p1] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 31 May 2016 ]
    token = security
    description = [ CVE-2016-5117 ]
]]

media-gfx/GraphicsMagick[<1.3.33] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Jul 2019 ]
    token = security
    description = [ CVE-2019-11473, CVE-2019-11474, CVE-2019-11505, CVE-2019-11506 ]
]]

app-arch/libarchive[<3.4.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Oct 2019 ]
    token = security
    description = [ CVE-2019-18408 ]
]]

(
    dev-lang/node[<12.15.0]
    dev-lang/node[>=13&<13.8.0]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 14 Feb 2020 ]
    *token = security
    *description = [ CVE-2019-15604, CVE-2019-15605, CVE-2019-15606 ]
]]

app-arch/p7zip[<15.14.1-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Jun 2016 ]
    token = security
    description = [ CVE-2016-2334, CVE-2016-2335 ]
]]

sys-devel/flex[<2.6.1-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 Aug 2016 ]
    token = security
    description = [ CVE-2016-6354 ]
]]

app-arch/tar[<1.31] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jan 2018 ]
    token = security
    description = [ CVE-2018-20482 ]
]]

dev-lang/guile:1.8[<1.8.8-r3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 22 Dec 2016 ]
    token = security
    description = [ CVE-2016-8605 ]
]]

dev-lang/guile:2[<2.0.13] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Nov 2016 ]
    token = security
    description = [ CVE-2016-8605 CVE-2016-8606 ]
]]

net-dialup/ppp[<2.4.7-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Nov 2016 ]
    token = security
    description = [ CVE-2015-3310 ]
]]

sys-libs/cracklib[<2.9.6-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 Dec 2016 ]
    token = security
    description = [ CVE-2015-6318 ]
]]

sys-libs/musl[<1.1.16] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Jan 2017 ]
    token = security
    description = [ CVE-2016-8859 ]
]]

media-libs/jbig2dec[<0.13-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 May 2017 ]
    token = security
    description = [ CVE-2017-{7885,7975,7976,9216} ]
]]

app-text/ghostscript[<9.27] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Apr 2019 ]
    token = security
    description = [ CVE-2019-{3835,3838,6116} ]
]]

sys-fs/ntfs-3g_ntfsprogs[<2016.2.22-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Feb 2017 ]
    token = security
    description = [ CVE-2017-0358 ]
]]

app-editors/vim[<8.1.1365] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 05 Jun 2019 ]
    token = security
    description = [ CVE-2019-12735 ]
]]

sys-apps/shadow[<4.5-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Feb 2018 ]
    token = security
    description = [ CVE-2018-7169 ]
]]

net-dns/c-ares[<1.13.0] [[
    author = [ Arnaud Lefebvre <a.lefebvre@outlook.fr> ]
    date = [ 12 Jul 2017 ]
    token = security
    description = [ CVE-2017-1000381 ]
]]

(
    dev-libs/libressl:42.44.16[<2.6.5]
    dev-libs/libressl:43.45.17[<2.7.4]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 13 Jun 2018 ]
    *token = security
    *description = [ CVE-2018-0495, CVE-2018-0732 ]
]]

net-libs/libtirpc[<1.0.2-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Mar 2018 ]
    token = security
    description = [ CVE-2016-4429 ]
]]

(
    dev-lang/go[<1.12.16]
    dev-lang/go[>=1.13&<1.13.7]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 30 Jan 2020 ]
    *token = security
    *description = [ CVE-2020-0601, CVE-2020-7919 ]
]]

dev-libs/libbsd[<0.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Feb 2016 ]
    token = security
    description = [ CVE-2016-2090 ]
]]

net-directory/openldap[<2.4.44-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 May 2017 ]
    token = security
    description = [ CVE-2017-9287 ]
]]

sys-devel/binutils[<2.28-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jun 2017 ]
    token = security
    description = [ CVE-2017-6969, CVE-2017-6966, CVE-2017-6965, CVE-2017-9041, CVE-2017-9040,
                    CVE-2017-9042, CVE-2017-9039, CVE-2017-9038, CVE-2017-8421, CVE-2017-8396,
                    CVE-2017-8397, CVE-2017-8395, CVE-2017-8394, CVE-2017-8393, CVE-2017-8398,
                    CVE-2017-7614 ]
]]

app-arch/unrar[<5.5.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 23 Jun 2017 ]
    token = security
    description = [ CVE-2012-6706 ]
]]

dev-scm/mercurial[<4.3.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Aug 2017 ]
    token = security
    description = [ CVE-2017-1000115, CVE-2017-1000116 ]
]]

dev-scm/subversion[<1.9.7] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Aug 2017 ]
    token = security
    description = [ CVE-2017-9800 ]
]]

sys-apps/coreutils[<8.28] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 2 Sep 2017 ]
    token = security
    description = [ CVE-2017-7476 ]
]]

app-text/podofo[<0.9.5_p20170903] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Sep 2017 ]
    token = security
    description = [ CVE-2017-{5852,5853,5854,5855,5886,6840,6844,6847,7378,
                              7379,7380,7994,8787} ]
]]

net-dns/dnsmasq[<2.78] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Oct 2017 ]
    token = security
    description = [ CVE-2017-13704, CVE-2017-1449{1,2,3,4,5,6} ]
]]

mail-mta/exim[<4.92.2-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 28 Sep 2019 ]
    token = security
    description = [ CVE-2019-16928 ]
]]

net-misc/rsync[<3.1.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Jan 2018 ]
    token = security
    description = [ CVE-2018-5764 ]
]]

sys-libs/ncurses[<6.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Feb 2018 ]
    token = security
    description = [ CVE-2017-{10684,10685,11112,11113,13728,13729,13730,
                              13731,13732,13733,13734,16879} ]
]]

app-arch/gcab[<1.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Feb 2018 ]
    token = security
    description = [ CVE-2018-5345 ]
]]

net-irc/irssi[<1.1.1] [[
    author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    date = [ 26 Feb 2018 ]
    token = security
    description = [ CVE-2018-705{0..4} ]
]]

net-dns/idnkit[>=2.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Mar 2018 ]
    token = broken
    description = [ Breaks its solely users bind{,-tools} ]
]]

(
    dev-db/postgresql[<9.3.22]
    dev-db/postgresql[>=9.4&<9.4.17]
    dev-db/postgresql[>=9.5&<9.5.12]
    dev-db/postgresql[>=9.6&<9.6.8]
    dev-db/postgresql[>=10&<10.3]
) [[
    *author = [ Arnaud Lefebvre <a.lefebvre@outlook.fr> ]
    *date = [ 08 Mar 2018 ]
    *token = security
    *description = [ CVE-2018-1058, https://wiki.postgresql.org/wiki/A_Guide_to_CVE-2018-1058%3A_Protect_Your_Search_Path ]
]]

net-misc/dhcp[<4.3.6_p1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Mar 2018 ]
    token = security
    description = [ CVE-2018-573{2,3} ]
]]

sys-apps/util-linux[<2.32] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 24 Mar 2018 ]
    token = security
    description = [ CVE-2018-7738 ]
]]

(
    dev-lang/php:7.2[<7.2.30]
    dev-lang/php:7.3[<7.3.17]
    dev-lang/php:7.4[<7.4.5]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 27 Apr 2020 ]
    *token = security
    *description = [ CVE-2020-706{4,5,6} ]
]]

sys-apps/busybox[<1.28.4-r1] [[
    *author = [ Kylie McClain <somasis@exherbo.org> ]
    *date = [ 7 Jun 2018 ]
    *token = security
    *description = [ Using busybox wget is insecure for https urls, as there is
                     no certificate validation done. Busybox >=1.28.4-r1 changes
                     configuration options to disable HTTPS functionality and
                     error when used. ]
]]

sys-process/procps[<3.3.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Jun 2018 ]
    token = security
    description = [ CVE-2018-{1122,1123,1124,1125,1126} ]
]]

dev-libs/crossguid[>=0.2.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jul 2018 ]
    token = testing
    description = [ Breaks its solely user Kodi ]
]]

app-arch/sharutils[<4.15.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Aug 2018 ]
    token = security
    description = [ CVE-2018-1000097 ]
]]

app-arch/cabextract[<1.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Nov 2018 ]
    token = security
    description = [ CVE-2018-18584 ]
]]

dev-util/elfutils[<0.176] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Feb 2019 ]
    token = security
    description = [  CVE-2019-{7146,7148,7149,7150,7664,7665} ]
]]

sys-devel/gettext[<0.19.8.1-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Jan 2019 ]
    token = security
    description = [ CVE-2018-18751 ]
]]

net-libs/ldns[<1.7.0-rc1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 05 Feb 2019 ]
    token = security
    description = [ CVE-2017-1000231, CVE-2017-1000232 ]
]]

sys-libs/libseccomp[<2.4.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Mar 2019 ]
    token = security
    description = [ https://www.openwall.com/lists/oss-security/2019/03/15/1 ]
]]

sys-process/cronie[<1.5.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Apr 2019 ]
    token = security
    description = [ CVE-2019-9704, CVE-2019-9705 ]
]]

dev-lang/perl:5.30 [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 22 May 2019 ]
    token = testing
    description = [ Might kill your kittens ]
]]

dev-libs/zziplib[<0.13.69-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Jun 2019 ]
    token = security
    description = [ CVE-2018-16548, CVE-2018-17828 ]
]]

dev-libs/glib:2[<2.62.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Feb 2020 ]
    token = security
    description = [ CVE-2020-6750 ]
]]

dev-db/mysql [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 May 2019 ]
    token = pending-removal
    description = [ Unmaintained on Exherbo, use MariaDB instead ]
]]

sys-libs/musl[<1.1.23-r1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 10 Aug 2019 ]
    token = security
    description = [ CVE-2019-14697 ]
]]

media-gfx/graphviz[<2.42.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Aug 2019 ]
    token = security
    description = [ CVE-2018-10196 ]
]]

(
    dev-lang/ruby:2.4[<2.4.7]
    dev-lang/ruby:2.5[<2.5.6]
    dev-lang/ruby:2.6[<2.6.4]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 17 Sep 2019 ]
    *token = security
    *description = [ CVE-2012-6708, CVE-2015-9251 ]
]]

dev-lang/python:3.8 [[
    author = [ Alexander Kapshuna <kapsh@kap.sh> ]
    date = [ 22 Sep 2019 ]
    token = testing
    description = [ Might break some packages ]
]]

dev-libs/libpcap[<1.9.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 06 Oct 2019 ]
    token = security
    description = [ CVE-2018-16301, CVE-2019-{15161,15162,15163,15164,15165} ]
]]

dev-cpp/gtest[~1.10.0] [[
    author = [ Paul Mulders <justinkb@gmail.com> ]
    date = [ 08 Oct 2019 ]
    token = testing
    description = [ Might break some dependents ]
]]

net-dns/libidn2[<2.2.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 Oct 2019 ]
    token = security
    description = [ CVE-2019-12290 ]
]]

app-arch/cpio[<2.13] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 06 Nov 2019 ]
    token = security
    description = [ CVE-2015-1197, CVE-2016-2037, CVE-2019-14866 ]
]]

dev-libs/fribidi[<1.0.7-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 09 Nov 2019 ]
    token = security
    description = [ CVE-2019-18397 ]
]]

dev-libs/oniguruma[<6.9.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Dec 2019 ]
    token = security
    description = [ CVE-2019-19{012,203,204,246} ]
]]

app-shells/zsh[<5.8] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 16 Feb 2020 ]
    token = security
    description = [ CVE-2019-20044 ]
]]

dev-lang/go[>=1.14] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Feb 2020 ]
    token = testing
    description = [ Needs some testing before unmasking ]
]]

(
    sys-devel/gcc:10
    sys-libs/libatomic:10
    sys-libs/libgcc:10
    sys-libs/libgfortran:10
    sys-libs/libgomp:10
    sys-libs/libsanitizer:10
    sys-libs/libstdc++:10
    sys-libs/libquadmath:10
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 27 Mar 2020 ]
    *token = toolchain-dev
    *description = [ Upcoming GCC release ]
]]

dev-libs/libressl:46.48.20 [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 09 Apr 2020 ]
    token = testing
    description = [ Development version, also might break stuff ]
]]

dev-libs/boost[>=1.73.0_beta1] [[
    author = [ Marvin Schmidt <marv@exherbo.org> ]
    date = [ 13 Apr 2020 ]
    token = testing
    description = [ New versions are likely to break dependents ]
]]

dev-libs/icu:67.1 [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 23 Apr 2020 ]
    token = testing
    description = [ Breaks everything bundling v8 (e.g. node, qtwebengine, chromium) ]
]]

dev-lang/php:7.1 [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Apr 2020 ]
    token = pending-removal
    description = [ EOL ]
]]
