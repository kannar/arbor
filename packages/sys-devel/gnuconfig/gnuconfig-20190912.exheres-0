# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

commit=1912ca50411bb77fb2c610ef55dd91e332663de9
WORK=${WORKBASE}/config-${commit:0:7}

SUMMARY="GNU config.guess and config.sub scripts"
HOMEPAGE="https://savannah.gnu.org/projects/config"
DOWNLOADS="http://git.savannah.gnu.org/gitweb/?p=config.git;a=snapshot;h=${commit};sf=tgz -> ${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="parts: data documentation"

DEPENDENCIES=""

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/0001-config.sub-recognise-LLVM-canonicalised-windows-trip.patch )

src_prepare() {
    default

    # Test hardcode `gnu` in expected triples
    if [[ $(exhost --build) == *-*-*-musl* ]]; then
        # Exclude ARM/FreeBSD for which `config.guess` hardcodes -gnueabi{,hf}
        edo sed -e '/| Linux |/s/-gnu/-musl/' \
                -e '/| GNU\/.* |/s/-gnu/-musl/' \
                -i testsuite/config-guess.data
    fi
}

src_install() {
    dodir /usr/share/${PN}
    insinto /usr/share/${PN}
    insopts -m0755
    doins config.guess config.sub
    dodoc ChangeLog
    doman doc/config.guess.1 doc/config.sub.1

    expart data /usr/share/gnuconfig
    expart documentation /usr/share/doc /usr/share/man
}

