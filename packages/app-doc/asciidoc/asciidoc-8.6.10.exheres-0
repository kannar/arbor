# Copyright 2008 Ciaran McCreesh
# Copyright 2009 Mike Kelly
# Copyright 2013 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist='3' has_bin=true multibuild=false ] \
        github autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Text based document generation"
DESCRIPTION="
AsciiDoc is a text document format for writing notes, documentation, articles,
books, ebooks, slideshows, web pages, man pages and blogs. AsciiDoc files can
be translated to many formats including HTML, PDF, EPUB, man page.
AsciiDoc is highly configurable: both the AsciiDoc source file syntax and the
backend output markups (which can be almost any type of SGML/XML markup) can
be customized and extended by the user."

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="vim-syntax"

DEPENDENCIES="
    run:
        app-text/docbook-xml-dtd:4.5
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt

    suggestion:
        app-text/docbook-dsssl-stylesheets
"

UPSTREAM_DOCUMENTATION="
    https://powerman.name/doc/asciidoc   [[ description = [ Cheatsheet ] ]]
    https://asciidoc.org/userguide.html  [[ description = [ User Guide ] ]]
    https://asciidoc.org/faq.html        [[ description = [ FAQ ] ]]
"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/blob/${PV}/CHANGELOG.txt"

src_configure() {
    # https://github.com/asciidoc/asciidoc/issues/118
    edo sed -i -e '/a2x/s/\<python\>/python2/' "${WORK}"/Makefile.in

    default
}

src_install() {
    default

    if option vim-syntax; then
        insinto /usr/share/vim/vimfiles/syntax
        doins vim/syntax/${PN}.vim
    fi
}

