# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_configure src_compile src_test src_install

SUMMARY="Open source SSL and TLS implementation and cryptographic library"
HOMEPAGE="https://www.openssl.org"
DOWNLOADS="${HOMEPAGE}/source/${PNV}.tar.gz"

UPSTREAM_CHANGELOG="${HOMEPAGE}/news/changelog.html [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs/ [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/news/announce.html [[ lang = en ]]"

LICENCES="${PN}"
SLOT="0"
MYOPTIONS="parts: binaries configuration development documentation libraries"

DEPENDENCIES="
    build+run:
        !dev-libs/libressl:* [[
            description = [ LibreSSL is a drop-in replacement for OpenSSL with same library name ]
            resolution = uninstall-blocked-after
        ]]
"

# Tests don't run in parallel
DEFAULT_SRC_TEST_PARAMS=( -j1 )

_openssl_host_os() {
    # local configuration=(
    #   alpha64:linux-alpha-gcc # TODO(compnerd) Alpha64 ev56+ needs -mcpu, linux-alpha+bwx-gcc
    #   amd64:linux-x86_64
    #   arm:linux-armv4 # TODO(compnerd) do we want to port android armv7 to Linux?
    #   ia64:linux-ia64 # TODO(compnerd) ICC should be linux-ia64-icc
    #   ppc:linux-ppc
    #   ppc64:linux-ppc64
    #   s390:linux${MULTIBUILD_TARGET}-s390x
    #   sparc:linux-sparcv9 # XXX(compnerd) do we even care about sparc v8?
    #   sparc64:linux64-sparcv9
    #   x86:linux-elf # TODO(compnerd) ICC should be linux-ia32-icc
    # )
    case "$(exhost --target)" in
    arm*-*-linux-*)
        echo linux-armv4
    ;;
    aarch64-*-linux-*)
        echo linux-aarch64
    ;;
    i686-*-linux-*)
        echo linux-elf
    ;;
    x86_64-*-linux-*)
        echo linux-x86_64
    ;;
    *)
        die "unknown OpenSSL host/os for $(exhost --target)"
    ;;
    esac
}

openssl_src_configure() {
    # LDFLAGS is out of the question thanks to the 'clever':
    # DO_GNU_APP=LDFLAGS="$(CFLAGS) -Wl,-rpath,$(LIBRPATH)"
    # We would like to use SHARED_LDFLAGS but that only works for .so's
    # So instead we just cheat and add LDFLAGS to CFLAGS so it gets
    # everywhere it needs to be along with some places where its harmless.
    export CFLAGS="${CFLAGS} ${LDFLAGS}"

    edo "${WORK}"/Configure $(_openssl_host_os) \
        --test-sanity

    edo "${WORK}"/Configure $(_openssl_host_os) \
        --prefix=/usr/$(exhost --target) --openssldir=/etc/ssl shared threads
}

openssl_src_compile() {
    # TODO(compnerd) fix parallel build
    local mymake=(
        -j1
        LIBDIR=lib
        MANDIR=/usr/share/man
    )

    if ever at_least 1.1; then
        mymake+=(
            ENGINESDIR=/usr/$(exhost --target)/lib/engines-1.1
        )
    else
        # NOTE(tridactyla) openssl uses CROSS_COMPILE to test whether to build the rehash target (which
        # runs newly built executables). If we set it through Configure, it prepends the value to all of
        # the tools (which already have the correct prefix), so set it here instead.
        mymake+=(
            $(exhost --is-native -q || echo CROSS_COMPILE=1)
        )
    fi

    emake "${mymake[@]}"
}

openssl_src_test() {
    esandbox allow_net --connect "inet:127.0.0.1@65535"
    default
    esandbox disallow_net --connect "inet:127.0.0.1@65535"
}

openssl_src_install() {
    local myinstall=(
        -j1
        LIBDIR=lib
        MANDIR=/usr/share/man
    )

    if ever at_least 1.1; then
        myinstall+=(
            DESTDIR="${IMAGE}"
            DOCDIR=/usr/share/doc/${PNVR}
            ENGINESDIR=/usr/$(exhost --target)/lib/engines-1.1
        )
    else
        myinstall+=(
            INSTALL_PREFIX="${IMAGE}"
        )
    fi

    emake "${myinstall[@]}" install

    # avoid file collisions with sys-apps/man-pages
    if ! ever at_least 1.1; then
        edo rm "${IMAGE}"/usr/share/man/man3/{err,rand}.3
    fi

    keepdir /etc/ssl/{certs,private}
    emagicdocs

    expart binaries /usr/$(exhost --target)/bin
    expart configuration /etc
    expart documentation /usr/share/{doc,man}
    expart libraries /usr/$(exhost --target)/lib
    expart development /usr/$(exhost --target)/{include,lib/pkgconfig}
}

