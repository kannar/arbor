# Copyright 2016 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=google project=googletest tag=release-${PV} ] cmake

export_exlib_phases src_prepare

SUMMARY="Google's framework for writing C++ tests on a variety of platforms, based on the xUnit architecture"
DESCRIPTION="
It supports automatic test discovery, a rich set of assertions, user-defined assertions, death tests,
fatal and non-fatal failures, various options for running the tests, and XML test report generation.
"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    googlemock [[ description = [ Framework for writing and using C++ mock classes ] ]]
    vim-syntax
"

DEPENDENCIES="
    build:
        dev-lang/python:*
    suggestion:
        vim-syntax? ( app-vim/googletest-syntax )
"

CMAKE_SOURCE=${WORKBASE}/googletest-release-${PV}

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -Dgtest_disable_pthreads:BOOL=FALSE
    -Dgtest_hide_internal_symbols:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    'googlemock GMOCK'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-Dgtest_build_tests=TRUE -Dgtest_build_tests=FALSE'
)

gtest_src_prepare() {
    cmake_src_prepare

    # gtest looks for its tests in ${WORK}/None because we set
    # CMAKE_BUILD_TYPE to None, but the test executeables are in ${WORK}
    edo sed "s/\$<CONFIGURATION>//" -i googletest/cmake/internal_utils.cmake
}

