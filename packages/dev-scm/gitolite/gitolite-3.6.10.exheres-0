# Copyright 2011 Ingmar Vanhassel
# Copyright 2012 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV=v${PV}
require github [ user=sitaramc tag=${MY_PV} ]

SUMMARY="Gitolite allows you to setup a centralised git server"
DESCRIPTION="
Gitolite allows you to setup a centralised git server, with very fine-grained access control and
many (many!) more powerful features.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/perl:=
        dev-scm/git[>=1.6.6]
        user/git
        group/git
"

BUGS_TO="mixi@exherbo.org"

src_compile() {
    :
}

src_install() {
    # install lib as vendorlib
    eval $(perl -V:vendorlib)
    insinto ${vendorlib}
    doins -r src/lib/Gitolite

    insinto /usr/$(exhost --target)/libexec/${PN}
    # get current version
    edo echo -n ${MY_PV} > VERSION
    doins VERSION
    # install source
    insopts -m0755
    doins -r src/{commands,syntactic-sugar,triggers,VREF}
    doins src/gitolite{,-shell}

    # symlink binaries
    dodir /usr/$(exhost --target)/bin
    for f in gitolite{,-shell}; do
        dosym /usr/$(exhost --target)/libexec/${PN}/${f} /usr/$(exhost --target)/bin/${f}
    done

    # install contrib samples
    insinto /usr/share/${PN}
    doins -r contrib/{commands,hooks,lib,triggers,utils}

    dodoc README.markdown CHANGELOG
}

pkg_postinst() {
    ewarn "Existing v2 installations are incompatible with this release."
    ewarn "Please consult http://gitolite.com/gitolite/migr.html on how to"
    ewarn "migrate old repositories and configuration."
}

